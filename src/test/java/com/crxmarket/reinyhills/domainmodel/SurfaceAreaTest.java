package com.crxmarket.reinyhills.domainmodel;

import com.crxmarket.rainyhills.domainmodel.SurfaceArea;
import org.junit.Assert;
import org.junit.Test;

/**
 * The SurfaceAreaTest is responsible for test the main algorithm of this application inside the SurfaceArea object.
 *
 * @author Leonardo Cruz
 *
 */
public class SurfaceAreaTest {


    private int calculateVolume(int[] sections) {
       SurfaceArea surfaceArea = new SurfaceArea();
       surfaceArea.setSections(sections);
       return surfaceArea.calculateRetainedVolume();
    }

    @Test
    public void testExampleOne() {
        Assert.assertEquals(2,calculateVolume(new int[]{3,2,4,1,2}));
    }

    @Test
    public void testExampleTwo() {
        Assert.assertEquals(8,calculateVolume(new int[]{4,1,1,0,2,3}));
    }

    @Test
    public void testEdgeCaseForZero1() {
        Assert.assertEquals(0,calculateVolume(new int[]{1}));
    }

    @Test
    public void testEdgeCaseForZero2() {
        Assert.assertEquals(0,calculateVolume(new int[]{1,1}));
    }

    @Test
    public void testEdgeCaseForZero3() {
        Assert.assertEquals(0,calculateVolume(new int[]{1,0}));
    }

    @Test
    public void testEdgeCaseForZero4() {
        Assert.assertEquals(0,calculateVolume(new int[]{0,1}));
    }

    @Test
    public void testEdgeCaseForZero5() {
        Assert.assertEquals(0,calculateVolume(new int[]{1,1,1}));
    }

    @Test
    public void testOneValley1() {
        Assert.assertEquals(1,calculateVolume(new int[]{1,0,1}));
    }

    @Test
    public void testOneValley2() {
        Assert.assertEquals(2,calculateVolume(new int[]{1,0,0,1}));
    }

    @Test
    public void testTwoValleys2() {
        Assert.assertEquals(2,calculateVolume(new int[]{1,0,1,0,1}));
    }

    @Test
    public void testPeakWithoutValleyOnLeftSide() {
        Assert.assertEquals(0,calculateVolume(new int[]{0,1,2}));
    }

    @Test
    public void testPeakWithoutValleyOnRightSide() {
        Assert.assertEquals(0,calculateVolume(new int[]{0,1,2}));
    }

    @Test
    public void testOneValleyWithAPeakOnLeftSide() {
        Assert.assertEquals(2,calculateVolume(new int[]{1,2,3,4,0,2}));
    }

    @Test
    public void testOneValleyWithAPeakOnRightSide() {
        Assert.assertEquals(2,calculateVolume(new int[]{2,0,4,3,2,1}));
    }

    @Test
    public void testOneCentralPeakWithoutValleys() {
        Assert.assertEquals(0,calculateVolume(new int[]{1,2,3,4,5,4,3,2,1}));
    }

    @Test
    public void testMixedTopography() {
        Assert.assertEquals(16,calculateVolume(new int[]{1,3,0,1,1,5,4,3,4,5,0,5,4}));
    }

    @Test
    public void testInsuficientSurfaceLenght() {
        Assert.assertEquals(0,calculateVolume(new int[]{1,2}));
    }

    @Test
    public void testNullSurface() {
        Assert.assertEquals(0,calculateVolume(null));
    }

    @Test
    public void testTwoSubSequentsCallsToCalculateRetainedVolume() {
        SurfaceArea surfaceArea = new SurfaceArea();
        surfaceArea.calculateRetainedVolume();
        surfaceArea.calculateRetainedVolume();
    }

}