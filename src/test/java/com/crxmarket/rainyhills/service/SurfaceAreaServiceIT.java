package com.crxmarket.rainyhills.service;

import com.crxmarket.rainyhills.domainmodel.SurfaceArea;
import com.crxmarket.rainyhills.logging.LoggingProvider;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

/**
 * Class responsible for integration tests of SurfaceAreaService
 *
 * @author Leonardo Cruz
 */
@RunWith(Arquillian.class)
public class SurfaceAreaServiceIT {

    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(SurfaceAreaService.class)
                .addClass(LoggingProvider.class)
                .addClass(SurfaceArea.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Inject
    private SurfaceAreaService surfaceAreaService;

    @Test
    public void testASimpleSurface() {

        int volume = surfaceAreaService.calculateRetainedVolume(new int[] {1,0,1});
        Assert.assertEquals(1,volume);

    }

}
