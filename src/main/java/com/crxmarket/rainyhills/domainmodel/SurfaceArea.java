package com.crxmarket.rainyhills.domainmodel;

/**
 * The SurfaceArea class represents a surface where water can be retained after a rain.
 *
 * <p>
 * <b>Usage:</b>
 * </p>
 * <pre>
 * {@code
 *
 *     SurfaceArea surfaceArea = new SurfaceArea();
 *     surfaceArea.setSections(array);
 *     int volume = surfaceArea.calculateRetainedVolume();
 *  }
 *</pre>
 * @author Leonardo Cruz
 *
 */
public class SurfaceArea {


    /**
     * The sections attribute represents the adjacent section of this surface area object.
     */
    private int[] sections;

    /**
     * This attribute represents the calculated volume
     */
    private Integer retainedVolume;

    /**
     * Defines the value of sections attribute
     *
     * @param sections of the surface
     */
    public void setSections(int[] sections) {
        this.sections = sections;
        retainedVolume = null;
    }

    /**
     * Calculates the retained volume of water based on the sections attribute. If sections attribute is null the calculated value will be zero.
     * @return calculated volume
     */
    public int calculateRetainedVolume() {
        if (retainedVolume == null) {
            retainedVolume = executeVolumeCalculation(sections);
        }
        return retainedVolume;
    }

    /**
     * Constructs a new surfaceArea object
     *
     */
    public SurfaceArea() {

    }

    /**
     * Calculates the volume based on defined surfaceModel and stores it on @{link volume} attribute
     *
     * @returns calculated volume
     */
    private int executeVolumeCalculation(int[] surfaceSections) {

        // We need at least three elements to form a valley and as a consequence retain water.
        if (surfaceSections == null || surfaceSections.length < 3) {
            return 0;
        }

        int currentValleyVolume = 0;
        int totalVolume = 0;

        int leftBarrierLevel = surfaceSections[0]; // Stores the left
        int leftBarrierIndex = -1;
        int delta = 0; // Stores the difference between two levels.

        //Iterates for the elements representing the levels of section of the surface
        for (int i = 1; i < surfaceSections.length; i++) {

            int currentSectionLevel = surfaceSections[i];
            delta = leftBarrierLevel - currentSectionLevel;

            if (delta > 0) { //Is there a gap in relation to the leftBarrierLevel
                currentValleyVolume += delta; // Accumulates the estimated volume for the current valley
            } else {
                //Adds the current valley's volume to the total volume
                totalVolume += currentValleyVolume;

                //Move to the next valley
                leftBarrierLevel = currentSectionLevel;
                leftBarrierIndex = i;
                currentValleyVolume = 0;
            }

        }

        //When we reach the final section and there is a right barrier that is smaller then the one one the left side
        //we need to adjust the previously calculated volume.

        if (currentValleyVolume > 0 && leftBarrierLevel > delta+1) {
            //If there is only one valley on the surface we need to initialize the leftBarrierIndex variable.
            if (leftBarrierIndex == -1) leftBarrierIndex = 0;
            //The overflow is the volume that could be retained on the left side but not on the right side.
            int  overflow = delta * (surfaceSections.length-1 - leftBarrierIndex);
            totalVolume += currentValleyVolume - overflow;
        }

        return totalVolume;

    }




}
