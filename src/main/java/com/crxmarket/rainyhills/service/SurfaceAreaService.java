package com.crxmarket.rainyhills.service;

import com.crxmarket.rainyhills.domainmodel.SurfaceArea;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.logging.Logger;

/**
 *
 * This enterprise bean is responsible to provide a service to execution of calculations related to
 * the retention of water on a surface.
 *
 * @author Leonardo Cruz
 *
 */
@Stateless
public class SurfaceAreaService {

    @Inject
    private Logger logger;


    /**
     * Calculates the value of the retained water volume for the informed surface
     *
     * @param surfaceSections an array of integers where each element represent a surface section's altitude
     * @return the calculated value
     *
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public int calculateRetainedVolume(int[] surfaceSections) {

        logger.finer("Calculating the retained volume. Input size " + surfaceSections.length);

        SurfaceArea surfaceArea = new SurfaceArea();
        surfaceArea.setSections(surfaceSections);
        return surfaceArea.calculateRetainedVolume();

    }

}
