package com.crxmarket.rainyhills.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * JaxRsActivator is a class responsible for the activation of the JAX-RS service and also for defining the root uri for REST requests.
 *
 * @author Leonardo Cruz
 */
@ApplicationPath("api/v1/")
public class JaxRsActivator extends Application {

}