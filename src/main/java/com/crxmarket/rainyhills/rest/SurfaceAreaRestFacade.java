package com.crxmarket.rainyhills.rest;

import com.crxmarket.rainyhills.service.SurfaceAreaService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * The SurfaceAreaRestFacade is responsible for expose the calculation service as a REST API.
 *
 * @author Leonardo Cruz
 */
@Path("surface")
public class SurfaceAreaRestFacade {

    @Inject
    private Logger logger;

    @Inject
    private SurfaceAreaService surfaceAreaService;

    /**
     * This method processes REST API calls to calculate the retained volume.
     * <p>
     * Example:
     * {@literal http://localhost:8080/test/api/v1/surface?section=1&section=0&section=1"}
     * </p>
     * <p>
     * Returns:
     * <code> {"volume":1} </code>
     * </p>
     * @param sections an array for with each section's altitude.
     * @return {@link javax.ws.rs.core.Response Response}
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response calculateRetainedVolume(@QueryParam("section") int[] sections) {

        logger.info("Processing HTTP request to calculate the retained volume.");
        try {
            Map<String,Integer> result = new HashMap<>();
            result.put("volume", surfaceAreaService.calculateRetainedVolume(sections));
            return Response.ok(result).build();
        } catch (RuntimeException e) {
            return Response.serverError().build();
        }

    }

}
