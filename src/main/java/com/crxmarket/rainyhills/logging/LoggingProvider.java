package com.crxmarket.rainyhills.logging;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import java.util.logging.Logger;

/**
 *
 * Provides a Logger instance to be injected on the application's classes
 *
 * @author Leonardo Cruz
 */
public class LoggingProvider {

    /**
     * Returns a Logger object based on the class where it will be injected.
     *
     * @param ip InjectionPoint
     * @return generated logger
     */
    @Produces
    public Logger createLogger(InjectionPoint ip) {
        return Logger.getLogger(ip.getMember().getDeclaringClass().getName());
    }
}